Buspirust is an experiment in converting protocols in a pipeline.

Currently it attempts to support BusPirate SPI and BBIO mode. Both not very successfully.

Requirements
------------

You need a BusPirate 3.5 with firmware 7.0 or higer for SPI to work.

The firmware tested was Community Firmware v7.1, and the flashing didn't succeed. Nevertheless, it somehow works so far.

Usage
-----

The command line is inconsistent, the code is in flux.

The first item is the name of the pipeline. There are several pipelines:

- serial - just serial port commands.
- bitbang - BusPirate's bitbang mode (over serial) (BP fw 6.3 seems to be mostly hosed, but serviceable with hacks I am not going to implement again)
- spi - BusPirate's binary SPI mode (over serial)
- progduino - pic32prog's "ansi" Arduino sketch protocol, over bitbang
- progduino:spi - same, over spi

Then, there are 2 big modes: interpret and transform. Interpret runs command streams. Transform "lowers" it to the next level in the pipeline and prints out.

The first argument is "(pipeline):interpret" or "(pipeline):transform".

The second argument is the stream to use. Some modes use `builtin://` prefix to use the data in the corresponding file, in module `streams`. No prefix means reading from file, unless it means using builtin :).

### Interpret

Interpret mode sometimes offers a REPL. Those expect an argument "watch" or "nowatch" in third position. The syntax of input is S-expressions, not the Rust syntax. Figure it out by using transform mode, or using serializer hints in the REPL.

Fourth argument is the serial port to open in all interpret modes based on serial (so all of them).

Example:

```
cargo run spi:interpret builtin://echo_long watch /dev/ttyUSB0
```

#### SPI mode

SPI mode doesn't seem to enter properly, so it needs to be prodded with:

```
cargo run serial:interpret builtin://start_bbio_safe /dev/ttyUSB0
```

This applies especially to the stream in `streams/spi.seriali`:

```
cargo run serial:interpret spi.seriali /dev/ttyUSB0
```

### Transform

Third position is sometimes occupied by "serialize" or "noserialize", depending on whether output is human-readable (Debug trait), or meant to be loaded again (in repl or from file).

Example of pretty printer:

```
# cargo run spi:transform builtin://echo no
...
Send(72)
Flush
Skip(1)
Send(16)
Send(1)
...
```

Serializable form:

```
# cargo run spi:transform builtin://echo serialize
...
(Send . 72)
Flush
(Skip . 1)
(Send . 16)
(Send . 1)
...
```

When translating to a lower form, **not all initialization is present**. The interpreters for different modes do some initialization on their own, like opening the device file, setting mode and so on. This initialization is not encoded in translations. The translated streams are pasteable in between each other though.

Architecture
------------

Ignore the first 500 lines of main.rs. The rest is messy due to adding features while trying to figure out what, if any, formal structure to give it.

So far, there are 5 main conceptual pieces:

- compiler ("transform_stream"), which lowers code
- analyzer ("transform_readouts"), which lifts readouts
- interpretes, which executes streams whichever way it wants and returns the readouts on the same level
- streams modules holding examples
- command line dispatcher which is non-uniformly hooked up
