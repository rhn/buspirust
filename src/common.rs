use std::ascii;

pub fn format_bytes(bytes: &[u8]) -> String {
    let bytes: Vec<_> = bytes.iter().map(|b| ascii::escape_default(*b)).flatten().collect();
    format!("{}", String::from_utf8_lossy(&bytes))
}


pub fn print_bytes(bytes: &[u8]) {
    println!("{}", format_bytes(bytes));
}

use std::cmp::PartialEq;

pub fn is_subslice<T: PartialEq>(haystack: &[T], needle: &[T]) -> bool {
    haystack.windows(needle.len()).any(|c| c == needle)
}

use std::collections::VecDeque;

pub struct Returns<I: Iterator> {
    iter: I,
    future_items: VecDeque<I::Item>, // LIFO
}

impl<I: Iterator> Returns<I> {
    pub fn next(&mut self) -> Option<I::Item> {
        self.future_items.pop_front()
            .or_else(|| self.iter.next())
    }
    pub fn place(&mut self, item: I::Item) {
        self.future_items.push_back(item)
    }
}

pub trait Returnable where Self: Sized + Iterator {
    fn returnable(self) -> Returns<Self>;
}

impl<T> Returnable for std::vec::IntoIter<T> {
    fn returnable(self) -> Returns<Self> {
        Returns {
            iter: self,
            future_items: VecDeque::new(),
        }
    }
}

pub use super::progduino::Interpreter;
pub use super::progduino::t::Command;


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn returnable_empty() {
        assert_eq!(Vec::<u8>::new().into_iter().returnable().next(), None);
    }

    #[test]
    fn returnable_nonempty() {
        assert_eq!(vec!(0u8).into_iter().returnable().next(), Some(0u8));
    }
    
    #[test]
    fn returnable_nonempty_pushed() {
        let mut r = vec!(0u8).into_iter().returnable();
        r.place(1u8);
        assert_eq!(r.next(), Some(1u8));
        assert_eq!(r.next(), Some(0u8));
        assert_eq!(r.next(), None);
    }
}

pub mod streams {
    use serde_lexpr;

    use std::io::{BufRead, Write};
    use serde::de::DeserializeOwned;
    use serde::ser::Serialize;

    /// One item per line
    pub fn to_writer<W: Write, C: Serialize>(mut w: W, stream: &[C]) -> serde_lexpr::Result<()> {
        for command in stream {
            serde_lexpr::to_writer(&mut w, command)?;
            w.write(b"\n")?;
        }
        Ok(())
    }
    
    pub fn from_line<C: DeserializeOwned>(line: &str) -> serde_lexpr::Result<C> {
        serde_lexpr::from_str(line)
    }
    
    /// One item per line.
    pub fn from_reader<R: BufRead, C: DeserializeOwned>(r: R) -> serde_lexpr::Result<Vec<C>> {
        let mut out = Vec::new();
        for line in r.lines() {
            let line = line?;
            let line = line.splitn(2, "#").nth(0).unwrap_or("");
            if line.len() > 0 {
                out.push(from_line(&line)?);
            }
        }
        Ok(out)
    }
}
