/*! BitBang mode for BusPirate */

use std::time::Duration;
use std::io;
use super::DeviceError;
use super::Level;
use super::Pirate;
use super::port;
use super::seriali;


use serde::{Serialize, Deserialize};

/* This is gonna be fun to generalize the command stream to different ISAs (!).
 * Needs Set/get/barrier, readout type.
 * Then have fun creating "stream" transformations:
 * Load/store/run/reset -> JTAG commands -> ICSP filter? -> bitbangs ->
 *                                                       \-> SPI   -/
 * -> BusPirate
 * or any other path.
 * 
 * If the ISA is smart enough, this can get us programmability with conditionals.
 * Feels like the '80s are coming back!
 */

/// Async bitbang pirate.
/// Pullups always on, power always on.
pub struct BitBang {
    port: serial::SystemPort,
    configs: PinConfigs,
    // Ideally, the bitbang executor knows how to avoid the quirks.
    // In some cases, this may cause a layering violation,
    // where things have to be handled on a higher level,
    // e.g. one pin needs to be reserved.
    quirks: Quirks,
}

#[derive(Clone)]
pub struct PinConfigs {
    // Internal banger state
    pub input_pins: u8,
    pub high_pins: u8,
}

pub enum Quirk {
    /// Delays bringing pins low until a high command is sent.
    Fw71,
}

pub type Quirks = Option<Quirk>;

impl BitBang {
    /// Initializes the BitBang mode.
    /// Doesn't try to perform reset.
    /// Sets POWER, PULLUP high, and the other pins in input.
    pub fn new(pirate: Pirate, quirks: Quirks) -> Result<BitBang, DeviceError> {
        // This command can leverage `seriali` now.
        let mut port = pirate.port;
        let mut reset = || {
            for _i in 0..20 {
                if let Ok(()) = port::command_quick(&mut port, b"\x00", b"BBIO1") {
                    return Some(());
                }
            }
            None
        };
        reset().ok_or(DeviceError::BBInit("Reset sequence failed to give bitbang mode"))?;

        println!("bitbang on");
        let levels = port::input_put(&mut port, 0x1f)
            .and_then(|_| port::level_put(&mut port, 0x60))?; // POWER, PULLUP
        println!("powerup {}", levels);
        Ok(BitBang {
            port,
            configs: PinConfigs {
                input_pins: 0x1f,
                high_pins: 0x60,
            },
            quirks,
        })
    }

    fn execute_serial(&mut self, stream: &[seriali::Command])
        -> (Vec<seriali::Readout>, Result<(), (io::Error, usize)>)
    {
        seriali::execute(&mut self.port, stream)
    }

    pub fn execute(&mut self, stream: &[Command])
        -> Result<Vec<Readout>, Error>
    {
        let (serial_stream, configs) = transform_stream(stream, self.configs.clone())
            .map_err(Error::StreamConversion)?;
        let (serial_readouts, result) = self.execute_serial(&serial_stream);
//        seriali::print_readouts(&serial_readouts);
        result.map_err(|(e, line)| Error::Execution(e, line))?;
        self.configs = configs;
        let readouts = transform_results(stream, &serial_readouts)
            .map_err(Error::ResultConversion)?;
        Ok(readouts)
    }

    pub fn get_quirks(&self) -> &Quirks {
        &self.quirks
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub enum Pin {
    CS =    0b1,
    MISO =  0b10,
    CLK =   0b100,
    MOSI =  0b1000,
    AUX =   0b10000,
    // Last two are not HiZ-capable
    //Pullup = 0b100000,
    Power = 0b1000000,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum Command {
    SetInput(Pin),
    SetHigh(Pin),
    SetLow(Pin),
    GetLevel(Pin),
    Wait(Duration),
    Barrier, // Prevents reordering of commands
}

pub type Readout = Level;


#[derive(Debug)]
pub enum Error {
    StreamConversion(()),
    Execution(io::Error, usize),
    ResultConversion(usize),
}


pub fn transform_results(stream: &[Command], results: &[seriali::Readout])
    -> Result<Vec<Level>, usize> // first invalid index
{
    let mut out = Vec::new();
    let mut idx = 0;
    // Take advantage of the fact
    // that only GetLevel does any non-discarded reads.
    let readouts = seriali::readouts_filter(results);

    for cmd in stream {
        use Command::*;
        match cmd {
            GetLevel(p) => out.push({
                let p = *p as u8;
                let readout = readouts.get(idx).ok_or(idx)?;
                idx += 1;
                match readout & p == 0 {
                    true => Level::Low,
                    false => Level::High,
                }
            }),
            _ => {},
        };
    }
    Ok(out)
}


/// Transform the stream into serial commands one command at a time.
///
/// Pin configs must be known beforehand
/// because each pin change command affects all pins.
///
/// Who's got the time to optimize?
/// But if/when, then this might benefit from research on lexers.
/// Just out basic tokens are not characters.
pub fn transform_stream(stream: &[Command], mut config: PinConfigs)
    -> Result<(Vec<seriali::Command>, PinConfigs), ()>
{
    let mut out = Vec::new();
    let mut read_bytes = 0;
    for cmd in stream {
        use Command::*;
        use seriali::Command::*;
        use seriali::Command as SC;
        match cmd {
            SetInput(p) => {
                let p = *p as u8;
                if (config.input_pins & p) == 0 {
                    config.input_pins |= p;
                    out.push(Send(0x40 | config.input_pins));
                    read_bytes += 1;
                }
            },
            SetHigh(p) => {
                let p = *p as u8;
                if (config.high_pins & p) == 0 {
                    config.high_pins |= p;
                    out.push(Send(0x80 | config.high_pins));
                    read_bytes += 1;
                }
                if (config.input_pins & p) == 0 {}
                else {
                    config.input_pins &= !p;
                    out.push(Send(0x40 | config.input_pins));
                    read_bytes += 1;
                }
            },
            SetLow(p) => {
                let p = *p as u8;
                if (config.high_pins & p) == 0 {}
                else {
                    config.high_pins &= !p;
                    out.push(Send(0x80 | config.high_pins));
                    read_bytes += 1;
                }
                if (config.input_pins & p) == 0 {}
                else {
                    config.input_pins &= !p;
                    out.push(Send(0x40 & config.input_pins));
                    read_bytes += 1;
                }
            },
            GetLevel(_p) => {
                // Let's not optimize yet. Get a new reading.
                out.push(Send(0x80 | config.high_pins));
                out.push(Flush);
                out.push(Skip(read_bytes));
                out.push(Read(1));
                read_bytes = 0;
            },
            Barrier => {
                // Okay, we still want computation to happen
                // when the next read is in the next stream.
                out.push(Flush);
                // No idea what the problem is :(
                // Sometimes BP will enter a state
                // it can't reset from without this,
                // especially with progduino CPUID test.
                // Changing IO timeout doesn't affect this.
                out.push(SC::Wait(Duration::from_millis(5)));
            },
            Command::Wait(d) => out.push(SC::Wait(*d)),
        };
    }
    Ok((out, config))
}


/** The firmware for BusPirate 7.1 has a broken implementation of bitbang.
 * 
 * This module transforms the command stream to fix misbehaviours.
 * 
 * The firmware seems to delay commands to lower the pin
 * until another pin is brought up.
 */
pub mod fw71 {
    use super::*;
    
    #[derive(Debug)]
    pub struct PinActuallyUsed;
    
    /// Fixes the glitch of pin going down after another going up.
    /// It's not a problem when there's no barrier between the two.
    /// To fix, this remembers whether any pin went down.
    /// Then it looks for either a pin up, or barrier.
    /// If there's a pin up, problem solved.
    /// If there's a barrier, insert pin up on an unused pin before the barrier.
    /// If the stream ends, insert pin up just in case.
    ///
    /// It seems the Bus Pirate doesn't care if the pin going up was already up,
    /// so no effort is made to bring it back down.
    pub fn transform_stream(stream: &[Command], unused: Pin)
        -> Result<Vec<Command>, PinActuallyUsed>
    {
        macro_rules! cp {
            ($p:expr) => {
                if $p == &unused { return Err(PinActuallyUsed) } else { }
            }
        }

        #[derive(PartialEq)]
        enum State {
            Normal,
            FoundSetLow,
            Finished,
        }
        use State::*;

        let mut out = Vec::new();
        let mut stream = stream.iter();

        let mut state = Normal;
        loop {
            state = match state {
                Normal => match stream.next() {
                    None => Finished,
                    Some(Command::SetLow(p)) => {
                        cp!(p);
                        out.push(Command::SetLow(*p));
                        FoundSetLow
                    },
                    Some(cmd) => {
                        out.push(cmd.clone());
                        Normal
                    },
                },
                FoundSetLow => match stream.next() {
                    None => {
                        // The stream is over,
                        // but without changing the pin level,
                        // the change may never happen.
                        // We don't know what's in the next stream.
                        // There may be a pin up, so this is a bit overcautious.
                        out.push(Command::SetHigh(unused));
                        Finished
                    },
                    Some(Command::SetHigh(p)) => {
                        cp!(p);
                        out.push(Command::SetHigh(*p));
                        Normal
                    },
                    Some(Command::Barrier) => {
                        out.push(Command::Barrier);
                        out.push(Command::SetHigh(unused));
                        out.push(Command::Barrier);
                        Normal
                    },
                    Some(cmd) => {
                        out.push(cmd.clone());
                        FoundSetLow
                    },
                },
                Finished => { break; },
            };
        }

        Ok(out)
    }
}


pub mod streams {
    use serde_lexpr;
    use std::time::Duration;
    use super::Command::*;
    use super::Pin::*;
    
    use std::io::{BufRead, Write};
    
    pub const LEVELS: &'static [super::Command] = &[
        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
    ];
    
    pub const BLINK: &'static [super::Command] = &[
        SetHigh(CS),
        SetHigh(MISO),
        SetHigh(CLK),
        SetHigh(MOSI),
        SetHigh(AUX),
        Barrier,
        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
        Wait(Duration::from_millis(500)),
        SetLow(CS),
        SetLow(MISO),
        SetLow(CLK),
        SetLow(MOSI),
        SetLow(AUX),
        Barrier,
        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
        Wait(Duration::from_millis(500)),
        SetHigh(CS),
        SetHigh(MISO),
        SetHigh(CLK),
        SetHigh(MOSI),
        SetHigh(AUX),
        Barrier,
        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
        Wait(Duration::from_millis(500)),
        SetLow(CS),
        SetLow(MISO),
        SetLow(CLK),
        SetLow(MOSI),
        SetLow(AUX),
        Barrier,
        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
    ];
    
    /// Light up in a pattern length 5 moving from one side to the other.
    pub const SNAKE: &'static [super::Command] = &[
        // Needed to set all pins to out I guess?
        SetHigh(CS),
        SetHigh(MISO),
        SetHigh(CLK),
        SetHigh(MOSI),
        SetHigh(AUX),
        Barrier,

        SetLow(CS),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(MISO),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(CLK),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(MOSI),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(AUX),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetHigh(CS),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetHigh(MISO),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetHigh(CLK),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetHigh(MOSI),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetHigh(AUX),
        Barrier,
    ];

    /// Failure at 6.3 firmware.
    pub const BAD_GRADUAL: &'static [super::Command] = &[
        SetInput(CS),
        SetInput(MISO),
        SetInput(CLK),
        SetInput(MOSI),
        SetInput(AUX),
        Barrier,
        
        SetLow(CS),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(MISO),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(CLK),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(MOSI),
        Barrier,
        Wait(Duration::from_millis(500)),
        SetLow(AUX),
        Barrier,

        GetLevel(CS),
        GetLevel(MISO),
        GetLevel(CLK),
        GetLevel(MOSI),
        GetLevel(AUX),
        // 6.3 firmware gives LHLHH , LLLLL expected
    ];
    
    /// Connect MOSI to MISO for better results.
    pub const SPI_SIMPLE: &'static [super::Command] = &[
        SetHigh(CS),
        SetInput(MISO),
        SetHigh(CLK),
        SetHigh(MOSI),
        SetHigh(AUX),
        Barrier,
        
        SetLow(CS), // enable device
        Barrier,
        Wait(Duration::from_millis(100)),
        SetLow(MOSI), // Send a bit
        Barrier,
        Wait(Duration::from_millis(100)),
        SetLow(CLK), // Communicate either on falling edge
        Barrier,
        GetLevel(CS), // low
        GetLevel(MISO), // low
        GetLevel(CLK), // low
        GetLevel(MOSI), // low
        GetLevel(AUX), // high
        Wait(Duration::from_millis(100)),
        SetHigh(CLK), // Or on raising edge
        Barrier,

        GetLevel(CS), // low
        GetLevel(MISO), // low
        GetLevel(CLK), // high
        GetLevel(MOSI), // low
        GetLevel(AUX), // high
        // 6.3 firmware passes
    ];

    /// ICSP test: CS, CLK like in SPI, MOSI bidirectional.
    /// Sends 1 bit, reads 1 bit, sends 1 bit.
    /// MOSI must not be connected apparently (6.3 fw).
    pub const ICSP_SIMPLE: &'static [super::Command] = &[
        SetHigh(CS),
        SetHigh(CLK),
        SetHigh(MOSI),

        SetHigh(MISO),
        SetHigh(AUX),
        Barrier,
        
        SetLow(CS), // enable device
        Barrier,
        Wait(Duration::from_millis(100)),
        SetLow(MOSI), // Send a bit
        Barrier,
        SetLow(CLK), // Communicate on falling edge
        Barrier,
        SetHigh(CLK),
        // Debug
        GetLevel(CS), // low
        GetLevel(CLK), // high
        GetLevel(MOSI), // low

        // Receive a bit
        SetInput(MOSI),
        Barrier,
        SetLow(CLK), // Use falling edge
        GetLevel(MOSI), // floating... should be HIGH
        Barrier,
        SetHigh(CLK),
        // Debug
        GetLevel(CS), // low
        GetLevel(CLK), // high
        GetLevel(MOSI), // high

        // Send another bit
        SetHigh(MOSI),
        Barrier,
        SetLow(CLK), // Use falling edge
        Barrier,
        SetHigh(CLK),
        Barrier,
        // Debug
        GetLevel(CS), // low
        GetLevel(CLK), // high
        GetLevel(MOSI), // high
        // 6.3 firmware passes
    ];

    pub fn get(name: &str) -> Option<&'static [super::Command]> {
        match name {
            "blink" => Some(BLINK),
            "levels" => Some(LEVELS),
            "snake" => Some(SNAKE),
            "bad_gradual" => Some(BAD_GRADUAL),
            "spi_simple" => Some(SPI_SIMPLE),
            "icsp_simple" => Some(ICSP_SIMPLE),
            _ => None,
        }
    }

    pub fn from_line(line: &str) -> serde_lexpr::Result<super::Command> {
        serde_lexpr::from_str(line)
    }

    /// One item per line.
    pub fn from_reader<R: BufRead>(r: R) -> serde_lexpr::Result<Vec<super::Command>> {
        let mut out = Vec::new();
        for line in r.lines() {
            out.push(from_line(&line?)?);
        }
        Ok(out)
    }
    
    /// One item per line
    pub fn to_writer<W: Write>(mut w: W, stream: &[super::Command]) -> serde_lexpr::Result<()> {
        for command in stream {
            serde_lexpr::to_writer(&mut w, command)?;
            w.write(b"\n")?;
        }
        Ok(())
    }
}
