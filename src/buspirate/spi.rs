/*! Bus Pirate SPI mode */

use crate::common;
use std::time::Duration;
use std::io;
use super::DeviceError;
use super::Pirate;
use super::port;
use super::seriali;


use crate::common::Interpreter;
use serde::{Serialize, Deserialize};


pub struct SPI {
    port: serial::SystemPort,
}

#[derive(Debug)]
pub enum InterpretError<C, E, A> {
    Compilation(C),
    Execution(E),
    Analysis(A),
}

pub type ExecuteError = InterpretError<CompilationError, (io::Error, usize), usize>;

impl SPI {
    /// Initializes the SPi mode.
    /// Sets POWER, PULLUP high, and the other pins in input.
    pub fn new(mut pirate: Pirate) -> Result<SPI, DeviceError> {
        let mut port = &mut pirate.port;
        let mut reset = || {
            for _i in 0..20 {
                if let Ok(()) = port::command_quick(&mut port, b"\x00", b"BBIO1") {
                    return Some(());
                }
            }
            None
        };
        reset().ok_or(DeviceError::BBInit("Reset sequence failed to give bitbang mode"))?;

        port::command_quick(&mut port, b"\x01", b"SPI1")?;

        println!("SPI on");

        // POWER on, 0, 0, CS high.
        port::command_quick(&mut port, &[0b01001001], b"\x01")?;
        
        port::command_quick(&mut port, &[0b10001011], b"\x01")?;
        // Speed 30kHz
        port::command_quick(&mut port, &[0b01100001], b"\x01")?;
        
        Ok(SPI { port: pirate.port })
    }
    
    fn get_state(&self) {}
    
    fn execute_lower(&mut self, stream: &[seriali::Command])
        -> Result<Vec<seriali::Readout>, (io::Error, usize)>
    {
        let (readouts, result) = seriali::execute(&mut self.port, stream);
        result.map(|()| readouts)
    }
    
    pub fn execute(&mut self, stream: &[Command])
        -> Result<Vec<Readout>, ExecuteError>
    {
        let backend_stream = transform_stream(stream, self.get_state())
            .map_err(InterpretError::Compilation)?;
        let result = self.execute_lower(&backend_stream);
        let readouts = result.map_err(InterpretError::Execution)?;
        let readouts = transform_readouts(stream, &readouts)
            .map_err(InterpretError::Analysis)?;
        Ok(readouts)
    }
}

impl Interpreter<Command> for SPI {
    type Error = ExecuteError;
    fn execute(&mut self, stream: &[Command]) -> Result<Vec<Readout>, ExecuteError> {
        self.execute(stream)
    }
}

#[derive(Debug)]
pub enum CompilationError { } // can't mess it up!

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Command {
    /// CS high
    Enable,
    /// CS low
    Disable,
    /// Reads the same number.
    Xfer(Vec<u8>),
    Wait(Duration),
}

impl common::Command for Command {
    type Readout = Readout;
}

pub type Readout = u8;

pub fn transform_stream(stream: &[Command], _state: ())
    -> Result<Vec<seriali::Command>, CompilationError>
{
    let mut out = Vec::new();
    for cmd in stream {
        use Command as C;
        use super::seriali::Command::*;
        use super::seriali::Command as SC;
        match cmd {
            C::Enable => {
                out.push(Send(0b11));
                out.push(Flush);
                out.push(Skip(1));
            },
            C::Disable => {
                out.push(Send(0b10));
                out.push(Flush);
                out.push(Skip(1));
            },
            C::Xfer(data) => {
                for next in data.chunks(16) {
                    out.push(Send(0b00010000 | (next.len() as u8 - 1)));
                    out.append(&mut next.iter().map(|b| Send(*b)).collect());
                    out.push(Flush);
                    out.push(Skip(1));
                    out.push(Read(next.len()));
                }
            },
            C::Wait(d) => out.push(SC::Wait(*d)),
        }
    }
    Ok(out)
}

pub fn transform_readouts(stream: &[Command], readouts: &[seriali::Readout])
    -> Result<Vec<Readout>, usize>
{
    let mut out = Vec::new();
    let mut idx = 0;
    let readouts = seriali::readouts_filter(readouts);
    for cmd in stream {
        use Command::*;
        match cmd {
            Xfer(writes) => {
                out.extend_from_slice(&readouts[idx..idx + writes.len()]);
                idx += writes.len()
                /*
                // MUST BE KEPT IN SYNC with transform
                for r in readouts[idx..idx + writes.len()].chunks(17) {
                    // If there's still data, there must be a first element.
                    let (_confirm, data) = r.split_first().unwrap();
                    idx += 1;
                    if data.len() == 0 {
                        return Err(idx); // SPI doesn't accept a read command that yields nothing.
                    }
                    out.extend_from_slice(data);
                    idx += data.len();
                }*/
            },
            _ => {},
        };
    }
    Ok(out)
}

pub mod streams {
    use super::Command;
    pub fn get(name: &str) -> Option<Vec<Command>> {
        use Command::*;
        match name {
            "noop" => Some(Vec::new()),
            "echo" => Some(vec!(
                Enable,
                Xfer(vec!(1)),
                Xfer(vec!(2, 3)),
                Xfer(vec!(5)),
                Xfer(vec!(17)),
            )),
            "echo_long" => Some(vec!(
                Enable,
                Xfer((0..18u8).collect()),
            )),
            _ => None,
        }
    }
}
