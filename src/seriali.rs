use serde::{Deserialize, Serialize};
use serial;
use std::io;
use std::thread::sleep;
use std::time::Duration;
use super::common::format_bytes;


use crate::ReadAll;
use std::io::Write;


#[derive(Debug, Deserialize, Serialize)]
pub enum Command {
    Send(u8),
    Skip(usize),
    Read(usize),
    ReadUntil(Vec<u8>),
    Wait(Duration),
    Flush, // it keeps send/read healthy. Is it a barrier?
}

#[derive(Clone)]
pub enum Readout {
    Saved(Vec<u8>),
    Skipped(Vec<u8>),
}

/// Removes all unsaved.
pub fn readouts_filter(results: &[Readout]) -> Vec<u8> {
    results
        .iter()
        .filter_map(|r| match r {
            Readout::Saved(v) => Some(v),
            _ => None,
        })
        .flatten()
        .map(|u| *u)
        .collect()
}

pub fn print_readouts(results: &[Readout]) {
    for r in results {
        use Readout::*;
        match r {
            Saved(bytes) => print!("{}", format_bytes(bytes)),
            Skipped(bytes) => print!("{}", format_bytes(bytes)),
        }
    }
    println!("");
}

fn dispatch(
    port: &mut serial::SystemPort,
    cmd: &Command,
    readouts: &mut Vec<Readout>
) -> Result<(), io::Error> {
    use Command::*;
    match cmd {
        &Send(byte) => {
            let mut arr = [0; 1];
            arr[0] = byte;
            port.write_all(&arr)?;
        },
        &Skip(count) =>  readouts.push(Readout::Skipped(port.read_exact_vec(count)?)),
        &Read(count) => readouts.push(Readout::Saved(port.read_exact_vec(count)?)),
        ReadUntil(ref s) => {
            let mut readout = Vec::new();
            loop {
                let mut out = port.read_exact_vec(1)?;
                readout.append(&mut out);
                if readout.ends_with(&s) {
                    break;
                }
            }
            readouts.push(Readout::Saved(readout));
        },
        &Wait(d) => sleep(d),
        &Flush => port.flush()?,
    };
    Ok(())
}

pub fn execute(mut port: &mut serial::SystemPort, stream: &[Command])
    -> (Vec<Readout>, Result<(), (io::Error, usize)>)
{
    let mut readouts = Vec::new();
    for (i, cmd) in stream.iter().enumerate() {
        if let Err(e) = dispatch(&mut port, cmd, &mut readouts) {
            return (readouts, Err((e, i)))
        }
    }
    (readouts, Ok(()))
}
