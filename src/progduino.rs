/*! Support for consuming the Arduino sketch commands from pic32prog.
 * Mostly piped through BusPirate.
 */
use std::collections::HashMap;
use std::time::Duration;
use super::buspirate;
use super::buspirate::bitbang;


#[derive(Debug)]
pub enum RuntimeError {
    Compilation(CompilationError),
    Execution(buspirate::bitbang::Error),
    Read(usize),
}

/// Interpreter for BusPirate bitbang mode
pub struct BBPirate {
    pirate: buspirate::BitBang,
    config: Config,
}

impl BBPirate {
    pub fn new(
        mut pirate: buspirate::BitBang,
        pins: PinMapping,
        quirks_override: Option<buspirate::bitbang::Quirks>,
    ) -> Result<BBPirate, buspirate::bitbang::Error> {
        let quirks = quirks_override
            .as_ref()
            .unwrap_or(pirate.get_quirks())
            .as_ref()
            .map(|q| match q {
                buspirate::bitbang::Quirk::Fw71 => {
                    use buspirate::bitbang::Pin::*;
                    use std::collections::HashSet;
                    use maplit::hashset;
                    
                    use std::iter::FromIterator;

                    // MISO leads to hangups. Aux doesn't.
                    let all_pins = hashset!(&AUX, &CS, &CLK, &MOSI);
                    let used_pins = HashSet::from_iter(pins.values());
                    let unused = all_pins.difference(&used_pins)
                        .next().expect("No unused pins!");
                    Quirk::Fw71RaisePin(**unused)
                },
            });
        
        println!("Selected quirks: {:?}", quirks);
        // Actually needed... Otherwise PGC will go together with PGD :S.
        pirate.execute(&bitbang_streams::get("icsp_setup", &pins).unwrap())?;
        Ok(BBPirate {
            pirate,
            config: Config { pins, quirks },
        })
    }
    
    fn execute_bitbang(&mut self, stream: &[buspirate::bitbang::Command])
        -> Result<Vec<buspirate::bitbang::Readout>, buspirate::bitbang::Error>
    {
        self.pirate.execute(stream)
    }
    
    pub fn execute(&mut self, stream: &[Command])
        -> Result<Vec<Bit>, RuntimeError>
    {
        let bitbang_stream = transform_stream(stream, &self.config)
            .map_err(RuntimeError::Compilation)?;
        let result = self.execute_bitbang(&bitbang_stream);
        let bang_readouts = result.map_err(RuntimeError::Execution)?;
        let (_consumed_count, readouts) = transform_readouts(stream, &bang_readouts)
            .map_err(RuntimeError::Read)?;
        Ok(readouts)
    }
}

impl Interpreter<Command> for BBPirate {
    type Error = RuntimeError;
    fn execute(&mut self, stream: &[Command]) -> Result<Vec<Readout>, Self::Error> {
        self.execute(stream)
    }
}


#[derive(Debug)]
pub enum CompilationError {
    NoPin(Pin),
    UnknownCommand(Command),
    Quirks(bitbang::fw71::PinActuallyUsed),
}

impl From<Pin> for CompilationError {
    fn from(p: Pin) -> CompilationError {
        CompilationError::NoPin(p)
    }
}

impl From<bitbang::fw71::PinActuallyUsed> for CompilationError {
    fn from(e: bitbang::fw71::PinActuallyUsed) -> CompilationError {
        CompilationError::Quirks(e)
    }
}

pub type Command = u8;

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum Pin {
    Power,
    PGD, // Data
    PGC, // Clock
    MCLR, // Reset
}

// Whatever type works for now.
pub type PinMapping = HashMap<Pin, buspirate::bitbang::Pin>;

fn pin(map: &PinMapping, p: Pin) -> Result<buspirate::bitbang::Pin, Pin> {
    map.get(&p).map(|p| *p).ok_or(p)
}

#[derive(Debug, Clone)]
pub enum Bit {
    Zero,
    One,
}

fn clock_bit(bit: Bit, config: &PinMapping, out: &mut Vec<buspirate::bitbang::Command>)
    -> Result<(), CompilationError>
{
    use buspirate::bitbang::Command::*;
    use Pin::*;

    let clock = pin(&config, PGC)?;
    let pgd = pin(&config, PGD)?;

    out.push(match bit {
        Bit::Zero => SetLow(pgd),
        Bit::One => SetHigh(pgd),
    });
    out.push(SetHigh(clock));
    out.push(Barrier);
    out.push(SetLow(clock));
    out.push(Barrier);
    Ok(())
}

/// Clock in a ICSP sequence of TDI, TMS, out, unused.
fn clock_four(
    tdi: Bit,
    tms: Bit,
    config: &PinMapping,
    mut out: &mut Vec<buspirate::bitbang::Command>
) -> Result<(), CompilationError> {
    use buspirate::bitbang::Command::*;
    use Pin::*;

    clock_bit(tdi, config, &mut out)?;
    clock_bit(tms, config, &mut out)?;
    
    let clock = pin(&config, PGC)?;
    let pgd = pin(&config, PGD)?;
    
    // Manually read out that bit.
    out.push(SetInput(pgd));
    out.push(Barrier);
    out.push(SetHigh(clock));
    out.push(Barrier);
    // The TDO gets clocked out from the remote end on the rising edge.
    out.push(GetLevel(pgd));
    // Not sure how GetLevel merging semantics should work.
    // Merging it into the previous instruction == bad,
    // merging into next == bad.
    // So barrier it is.
    out.push(Barrier);
    out.push(SetLow(clock));
    out.push(Barrier);
    // Last bit ignored.
    // The Python version was explicitly clocking this in,
    // so better just follow it.
    out.push(SetHigh(clock));
    out.push(Barrier);
    out.push(SetLow(clock));
    out.push(Barrier);
    Ok(())
}

fn apply_cmd(
    cmd: &Command,
    config: &PinMapping,
    mut out: &mut Vec<buspirate::bitbang::Command>
) -> Result<(), CompilationError> {
    use buspirate::bitbang::Command::*;
    use Pin::*;
    match cmd {
        b'\n' => {}, // ignore newlines
        b'?' => { }, // It only prints '?', so noop for bitbang
        // Start power
        b'5' => {
            out.push(SetHigh(pin(&config, PGD)?));
            out.push(Barrier);
            out.push(SetHigh(pin(&config, Power)?));
            out.push(Barrier);
        },
        b'.' => {}, // comment
        b'8' => {
            out.push(Barrier);
            out.push(Wait(Duration::from_millis(20)));
            out.push(Barrier);
        },
        b'3' => {
            // originally HiZ, but Python version works
            out.push(SetHigh(pin(&config, MCLR)?));
            out.push(Barrier);
        },
        b'2' => {
            out.push(SetLow(pin(&config, MCLR)?));
            out.push(Barrier);
        },
        b'0' => clock_bit(Bit::Zero, config, &mut out)?,
        b'1' => clock_bit(Bit::One, config, &mut out)?,
        // Those don't read in. That could be optimized, but then optimized away.
        b'e' => clock_four(Bit::Zero, Bit::One, config, &mut out)?,
        b'd' => clock_four(Bit::Zero, Bit::Zero, config, &mut out)?,
        // Reads in a bit.
        b'D' => apply_cmd(&b'd', config, &mut out)?,
        b'A' => out.append(&mut transform_stream_base(b"edD", config)?),
        b'I' => out.append(&mut transform_stream_base(b"DDDD", config)?),
        other => return Err(CompilationError::UnknownCommand(*other)),
    };
    Ok(())
}

fn transform_stream_base(stream: &[Command], config: &PinMapping)
    -> Result<Vec<buspirate::bitbang::Command>, CompilationError>
{
    let mut out = Vec::new();
    for cmd in stream {
        apply_cmd(cmd, config, &mut out)?;
    }
    Ok(out)
}

#[derive(Debug)]
pub enum Quirk {
    Fw71RaisePin(buspirate::bitbang::Pin),
}

pub struct Config {
    pub pins: PinMapping,
    pub quirks: Option<Quirk>,
}

pub fn transform_stream(stream: &[Command], config: &Config)
    -> Result<Vec<buspirate::bitbang::Command>, CompilationError>
{
    let out = transform_stream_base(stream, &config.pins)?;
    let out = match config.quirks {
        Some(Quirk::Fw71RaisePin(pin)) => buspirate::bitbang::fw71::transform_stream(&out, pin)?,
        None => out,
    };
    Ok(out)
}

pub fn transform_readouts(stream: &[Command], results: &[buspirate::bitbang::Readout])
    -> Result<(usize, Vec<Bit>), usize> // consumed count, bits, first invalid idx
{
    let mut out = Vec::new();
    let mut idx = 0;

    let sub = |out: &mut Vec<_>, substream: &[_], idx: &mut usize| -> Result<(), usize> {
        let (consumed, mut subreads) = transform_readouts(substream, &results[*idx..])
            .map_err(|in_idx| *idx + in_idx)?;
        *idx += consumed;
        &out.append(&mut subreads);
        Ok(())
    };
    // 'd' and 'e' does a readout, but ignoreable one. By extension, A too
    for cmd in stream {
        match cmd {
            b'e' | b'd' => { idx += 1 },
            b'D' => {
                let level = results.get(idx).ok_or(idx)?;
                out.push(match level {
                    buspirate::Level::High => Bit::One,
                    buspirate::Level::Low => Bit::Zero,
                });
                idx += 1;
            },
            b'A' => sub(&mut out, b"edD", &mut idx)?,
            b'I' => sub(&mut out, b"DDDD", &mut idx)?,
            _ => {},
        }
    }
    Ok((idx, out))
}

#[derive(Debug)]
pub enum InterpretError<C, E, A> {
    Compilation(C),
    Execution(E),
    Analysis(A),
}

/// EXPERIMENTAL
pub trait ForwardingInterpreter<C: t::Command> {
    type Error: Debug;
    type BackendCommand;
    type Compiler: Compiler<C, Self::BackendCommand>;
    type Analyzer: Analyzer<C>;
    
    fn get_state(&self) -> &<Self::Compiler as Compiler<C, Self::BackendCommand>>::Config;
    
    fn execute_lower(&mut self, stream: &[Self::BackendCommand])
        -> Result<Vec<<Self::Analyzer as Analyzer<C>>::BackendReadout>, Self::Error>;
    
    fn execute(&mut self, stream: &[C])
        -> Result<
            Vec<C::Readout>,
            InterpretError<
                <Self::Compiler as Compiler<C, Self::BackendCommand>>::Error,
                Self::Error,
                <Self::Analyzer as Analyzer<C>>::Error,
            >
        >
    {
        let backend_stream = Self::Compiler::transform_stream(stream, self.get_state())
            .map_err(InterpretError::Compilation)?;
        let result = self.execute_lower(&backend_stream);
        let readouts = result.map_err(InterpretError::Execution)?;
        let readouts
            = <Self::Analyzer as Analyzer<C>>::transform_readouts(stream, &readouts)
                .map_err(InterpretError::Analysis)?;
        Ok(readouts)
    }
}

use std::fmt::Debug;

pub mod t {
    use std::fmt::Debug;
    pub trait Command {
        type Readout: Debug;
    }
}

type Readout = Bit;

impl t::Command for Command {
    type Readout = Readout;
}

pub trait Interpreter<C: t::Command> {
    type Error: Debug;
    fn execute(&mut self, stream: &[C])
        -> Result<Vec<C::Readout>, Self::Error>;
}

pub struct SPIPirate {
    pirate: buspirate::spi::SPI,
}

impl SPIPirate {
    pub fn new(pirate: buspirate::spi::SPI) -> SPIPirate {
        SPIPirate { pirate }
    }
}

pub type SPIExecuteError = InterpretError<SPICompilationError, buspirate::spi::ExecuteError, ()>;

impl<T, C: t::Command> Interpreter<C> for T where T: ForwardingInterpreter<C> {
    type Error = InterpretError<
        <T::Compiler as Compiler<C, T::BackendCommand>>::Error,
        T::Error,
        <T::Analyzer as Analyzer<C>>::Error,
    >;
    fn execute(&mut self, stream: &[C])
        -> Result<Vec<C::Readout>, Self::Error>
    {
        <Self as ForwardingInterpreter<C>>::execute(self, stream)
    }
}

impl ForwardingInterpreter<Command> for SPIPirate {
    type BackendCommand = buspirate::spi::Command;
    type Compiler = SPIBackend;
    type Analyzer = SPIBackend;
    type Error = buspirate::spi::ExecuteError;
    
    fn get_state(&self) -> &() { &() }

    fn execute_lower(&mut self, stream: &[buspirate::spi::Command])
        -> Result<Vec<buspirate::spi::Readout>, Self::Error>
    {
        let o = self.pirate.execute(stream)?;
        Ok(o)
    }
}

        
#[derive(Clone, Copy, Hash)]
enum BitXfer {
    One,
    Zero,
    Read,
}

impl BitXfer {
    fn to_int(self, pos: u8) -> u8 {
        use BitXfer::*;
        let bit = match self {
            One => 1,
            Zero => 0,
            // Doesn't matter, but 1's are easier to spot in the logic analyzer.
            Read => 1,
        };
        bit << pos
    }
}

fn extract_reads(cmd: &Command) -> &'static [BitXfer] {
    use BitXfer::*;
    match cmd {
        b'0' => &[Zero],
        b'1' => &[One],
        b'd' => &[Zero, Zero, Zero, Zero],
        b'e' => &[Zero, One, Zero, Zero],
        // Read should be on the 3rd position,
        // but it actually shows up between the 3rd and 4th falling clock edge.
        // BusPirate picks that up in the 4th position.
        b'D' => &[Zero, Zero, Zero, Read],
        _ => &[],
    }
}

/// EXPERIMENTAL
/// Transforms readouts into a higher level representation.
pub trait Analyzer<C: t::Command> {
    type BackendReadout;
    type Error: Debug;
    
    fn transform_readouts(stream: &[C], readouts: &[Self::BackendReadout])
        -> Result<Vec<C::Readout>, Self::Error>;
}

impl Analyzer<Command> for SPIBackend {
    type BackendReadout = buspirate::spi::Readout;
    type Error = ();

    fn transform_readouts(stream: &[Command], readouts: &[Self::BackendReadout])
        -> Result<Vec<Readout>, Self::Error>
    {
        let mut bits = Vec::with_capacity(readouts.len() * 8);
        for byte in readouts {
            for i in (0..8).rev() {
                bits.push(match byte & (1 << i) {
                    0 => Bit::Zero,
                    _ => Bit::One,
                })
            }
        };

        let stream = flatten_stream(stream);
        Ok(stream.iter()
            .map(extract_reads)
            .flatten()
            .zip(bits)
            .filter_map(|(xfer, bit)| match xfer {
                BitXfer::Read => Some(bit),
                _ => None,
            })
            .collect())
    }
}

/// EXPERIMENTAL
pub trait Compiler<C, BC> {
    type Config;
    type Error: Debug;
    fn transform_stream(stream: &[C], config: &Self::Config)
        -> Result<Vec<BC>, Self::Error>;
}

#[derive(Debug)]
pub enum SPICompilationError {
    /// Number of reads/writes is not divisible by 8.
    MisalignedXfer,
    UnsupportedCommand(Command),
}

/// Something to implement the Compiler trait on.
pub struct SPIBackend;

impl Compiler<Command, buspirate::spi::Command> for SPIBackend {
    type Config = ();
    type Error = SPICompilationError;
    fn transform_stream(stream: &[Command], _config: &Self::Config)
        -> Result<Vec<buspirate::spi::Command>, Self::Error> 
    {
        use buspirate::spi;
        use maplit::hashset;

        use super::common::Returnable;
        
        let stream = flatten_stream(stream);
        let mut stream = stream.into_iter().returnable();
        let mut out_stream = Vec::new();

        enum State {
            ReadIn,
            CollectXfers(Vec<BitXfer>),
            Finished,
        }
        use State::*;
        let mut state = State::ReadIn;
        'main: loop {
            state = match state {
                ReadIn => match stream.next() {
                    None => Finished,
                    Some(b'?') => ReadIn,
                    Some(b'5') => ReadIn, // Power started by default
                    Some(b'8') => {
                        out_stream.push(spi::Command::Wait(Duration::from_millis(10)));
                        ReadIn
                    },
                    Some(b'3') => {
                        out_stream.push(spi::Command::Enable);
                        ReadIn
                    },
                    Some(b'2') => {
                        out_stream.push(spi::Command::Disable);
                        ReadIn
                    },
                    Some(cmd) => {
                        // Awkward. Rust doesn't support the pattern
                        // (cmd @ (a | b | c | d)) yet.
                        if hashset!(b'0', b'1', b'd', b'e', b'D').contains(&cmd) {
                            stream.place(cmd);
                            CollectXfers(Vec::new())
                        } else {
                            return Err(SPICompilationError::UnsupportedCommand(cmd));
                        }
                    },
                },
                CollectXfers(mut w) => {
                    loop {
                        match stream.next() {
                            Some(b'0') => w.extend_from_slice(extract_reads(&b'0')),
                            Some(b'1') => w.extend_from_slice(extract_reads(&b'1')),
                            Some(b'd') => w.extend_from_slice(extract_reads(&b'd')),
                            Some(b'e') => w.extend_from_slice(extract_reads(&b'e')),
                            Some(b'D') => w.extend_from_slice(extract_reads(&b'D')),
                            Some(other) => {
                                stream.place(other);
                                break;
                            },
                            None => { break; },
                        };
                    }
                    if w.len() % 8 == 0 {
                        let bytes = w.chunks(8).map(|octet| {
                            let mut sum = 0u8;
                            for (pos, bit) in octet.into_iter().enumerate() {
                                // SPI transfers MSB first.
                                sum += bit.to_int(7 - pos as u8);
                            }
                            sum
                        }).collect();
                        out_stream.push(spi::Command::Xfer(bytes));
                        ReadIn
                    } else {
                        return Err(SPICompilationError::MisalignedXfer);
                    }
                },
                Finished => { break 'main; },
            };
        }
        Ok(out_stream)
    }
}
    

fn flatten_stream(stream: &[Command]) -> Vec<Command> {
    let mut out = Vec::new();
    for cmd in stream {
        match cmd {
            b'A' => out.extend_from_slice(b"edD"),
            b'I' => out.extend_from_slice(b"DDDD"),
            b'.' => {},
            b'\n' => {},
            other => out.push(*other),
        }
    }
    out
}

pub mod bitbang_streams {
    use crate::buspirate::bitbang::Command;
    use super::{ pin, Pin, PinMapping };
    use std::time::Duration;

    pub fn get(name: &str, pins: &PinMapping) -> Option<Vec<Command>> {
        use Command::*;
        use Pin::*;
        let clock = pin(&pins, PGC).unwrap();
        let data = pin(&pins, PGD).unwrap();
        let reset = pin(&pins, MCLR).unwrap();
        
        match name {
            "icsp_setup" => Some(vec![
                SetHigh(data),
                SetLow(clock),
                Barrier,
                SetHigh(reset),
                Barrier,
                Wait(Duration::from_millis(100)),
            ]),
            _ => None,
        }
    }
}

pub mod streams {
    pub const NOOP: &'static [super::Command] = &[
    ];
    
    pub const BEGIN: &'static [super::Command] = b"?5.88888.32.8.0100.1101.0100.0011.0100.1000.0101.0000.8.3.8.e";
    
    pub const CPUID: &'static [super::Command] = b"?5.88888.32.8.0100.1101.0100.0011.0100.1000.0101.0000.8.3.8.eeeeedAIIIIIIIDDD";

    pub fn get(name: &str) -> Option<&'static [super::Command]> {
        match name {
            "noop" => Some(NOOP),
            "cpuid" => Some(CPUID),
            "begin" => Some(BEGIN),
            _ => None,
        }
    }
}
