mod buspirate;
mod common;
mod progduino;
mod seriali;

use common::print_bytes;
use maplit::hashmap;
use serial;
use std::env;
use std::ffi::OsString;
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::thread::sleep;
use std::time::Duration;


// Traits
use common::Interpreter;
use serde::de::DeserializeOwned;
use serial::SerialPort;
use std::fmt::Debug;
use std::io::{BufRead, Read, Write};


/// Helper procedures.
/// These seem to drop parts of the buffer on occasional error,
/// at least if the docs say the truth...
trait ReadAll where Self: Read {
    fn read_all(&mut self) -> (Vec<u8>, Result<(), io::Error>) {
        let mut data = Vec::new();
        loop {
            let batch_capacity = 64;
            let mut batch = Vec::with_capacity(batch_capacity);
            batch.resize(64, 0);
            let result = self.read(&mut batch);
            // TODO: this eventually gives a timeout... maybe ignore them?
            match result {
                Err(e) => {
                    match e.kind() {
                        io::ErrorKind::TimedOut => return (data, Ok(())),
                        _ => return (data, Err(e)),
                    }
                },
                Ok(0) => {
                    return (data, Ok(()));
                },
                Ok(size) => {
                    batch.truncate(size);
                    data.append(&mut batch);
                },
            }
        }
    }
    
    fn read_exact_vec(&mut self, count: usize) -> io::Result<Vec<u8>> {
        let mut data = Vec::with_capacity(count);
        data.resize(count, 0);
        self.read_exact(&mut data)?;
        Ok(data)
    }
}

impl ReadAll for serial::SystemPort {}

#[derive(Debug)]
enum Error {
    Serial(serial::Error),
    BusPirate(BusPirateError),
    BitBangRetry,
    BP(buspirate::bitbang::Error),
    Device(buspirate::DeviceError),
    Progduino(progduino::RuntimeError),
    ProgduinoSPI(progduino::SPIExecuteError),
}

impl From<serial::Error> for Error {
    fn from(e: serial::Error) -> Error {
        Error::Serial(e)
    }
}

impl From<BusPirateError> for Error {
    fn from(e: BusPirateError) -> Error {
        Error::BusPirate(e)
    }
}

impl From<buspirate::bitbang::Error> for Error {
    fn from(e: buspirate::bitbang::Error) -> Error {
        Error::BP(e)
    }
}

impl From<buspirate::DeviceError> for Error {
    fn from(e: buspirate::DeviceError) -> Error {
        Error::Device(e)
    }
}


#[derive(Debug)]
pub enum BusPirateError {
    IO(io::Error),
    Discover,
    Reset(String),
    Reconfigure(String),
    OutPinLevel { differences: u8, got: u8 },
}

impl From<io::Error> for BusPirateError {
    fn from(e: io::Error) -> BusPirateError {
        BusPirateError::IO(e)
    }
}

/// Very synchronous bitbang
struct BitBangPirate {
    port: serial::SystemPort,
    /// Both track the presumable internal mechanisms of the BitBang,
    /// especially level has no relation
    /// to the read out value (shorts change it).
    /// Invariant: within 0x7f
    out_level: u8,
    /// Invariant: within 0x1f
    input_state: u8,
}

impl BitBangPirate {
    /// Internal use only.
    /// Maybe it should actually only accept a fully grown BusPirate.
    fn new(mut port: serial::SystemPort)
        -> Result<BitBangPirate, (serial::SystemPort, BusPirateError)>
    {
        for _i in 0..20 {
            let result = BusPirate::port_command(&mut port, b"\x00", b"BBIO1");
            if let Ok(()) = result {
                let result = BitBangPirate::port_input_put(&mut port, 0x1f)
                    .and_then(|_| BitBangPirate::port_level_put(&mut port, 0));
                return match result {
                    Err(e) => Err((port, e.into())),
                    Ok(out_level) => Ok(BitBangPirate {
                        port,
                        out_level: out_level & 0x7f & !0x1f, // only outputs
                        input_state: 0x1f, // hopefully this actually happens
                    })
                };
            }
        }
        Err((port, BusPirateError::Reconfigure(String::from("Failed to init BBIO"))))
    }

    fn port_input_put(port: &mut serial::SystemPort, pins: u8) -> io::Result<u8> {
        port.write(&[0x40 | (pins & 0x1f)])?;
        port.flush()?; // this is kinda silly. Should be turned async (command stream?)
        sleep(Duration::from_millis(10));
        BitBangPirate::port_consume_pins_byte(port)
    }
    
    fn port_level_put(port: &mut serial::SystemPort, pins: u8) -> io::Result<u8> {
        port.write(&[0x80 | (pins & 0x7f)])?;
        port.flush()?;
        sleep(Duration::from_millis(10));
        BitBangPirate::port_consume_pins_byte(port)
    }

    fn outputting_pins(&self) -> u8 {
        0x7f & !(self.input_state & 0x1f)
    }

    /// Interprets the single byte with pin info that was left
    /// just after a call to change pin state.
    fn port_consume_pins_byte(port: &mut serial::SystemPort) -> io::Result<u8> {
        let bytes = port.read_exact_vec(1)?;
        Ok(bytes[0] & 0x7f)
    }

    /// Uses provided value as the new pin value, returns set pins.
    pub fn level_put(&mut self, pins: u8) -> Result<u8, BusPirateError> {
        self.out_level = pins & 0x7f;
        let highs = BitBangPirate::port_level_put(&mut self.port, self.out_level)
            .map_err(BusPirateError::IO)?;
        self.check_out_levels(highs)
    }

    pub fn level_set(&mut self, pins: u8) -> Result<u8, BusPirateError> {
        self.level_put(self.out_level | pins)
    }

    pub fn level_clear(&mut self, pins: u8) -> Result<u8, BusPirateError> {
        self.level_put(self.out_level & !pins)
    }
    
    fn check_out_levels(&self, highs: u8) -> Result<u8, BusPirateError> {
        let high_out_seen = highs & self.outputting_pins();
        let high_out_wanted = self.out_level & self.outputting_pins();
        let differences = high_out_seen ^ high_out_wanted;
        if differences == 0 {
            Ok(highs)
        } else {
            Err(BusPirateError::OutPinLevel { differences, got: highs })
        }
    }
    
    pub fn input_set(&mut self, pins: u8) -> Result<u8, BusPirateError> {
        self.input_state |= pins & 0x1f;
        let highs = BitBangPirate::port_input_put(&mut self.port, self.input_state)?;
        self.check_out_levels(highs)
    }

    pub fn input_clear(&mut self, pins: u8) -> Result<u8, BusPirateError> {
        self.input_state &= !pins & 0x1f;
        let highs = BitBangPirate::port_input_put(&mut self.port, self.input_state)?;
        self.check_out_levels(highs)
    }
}

pub struct BusPirate {
    port: serial::SystemPort,
}

impl BusPirate {
    fn new(mut port: serial::SystemPort)
        -> Result<(Vec<u8>, BusPirate), BusPirateError> // return version buffer alongside
    {
    /*    if let Err(e) = port.set_timeout(Duration::from_millis(10)) {
            eprintln!("Warning: timeout not set. May be slow or unstable. {}", e);
        }*/
        /* TODO: check pid/vid from
         * /sys/class/tty/ttyUSB0/device/subsystem -> ../../../../../../../bus/usb-serial
         * then realpath it to get down to /sys/devices/pci0000:00/0000:00:14.0/usb3/3-1/
         * then read idProduct and IdVendor.
         * 
         * Then read /usr/lib/python3.7/site-packages/serial/tools/list_ports*
         */
        
        if let Err(_) = BusPirate::port_into_bb(&mut port) {
            // Force reset by pushing 20× \0 in total
            let plunger_array: [u8; 19] = [0; 19];
            port.write(&plunger_array)?;
            port.flush()?;
            
            sleep(Duration::from_millis(100));
            let (out, result) = port.read_all();
            if let Err(e) = result {
                eprintln!("Warning: read problems when initializing BP: {}", e);
            }
            
            if !out.ends_with(b"BBIO1") {
                return Err(BusPirateError::Discover);
            }
        }

        port.write(b"\x0f")?;
        port.flush()?;
        sleep(Duration::from_millis(100)); // enough for a full reset
        let (mut out, result) = port.read_all();
        if let Err(e) = result {
            eprintln!("Warning: read problems when resetting BP: {}", e);
        }

        let resp_templ = b"\x01";
        let prompt_templ = b"HiZ>";
        if out.len() < resp_templ.len() + prompt_templ.len() {
            return Err(BusPirateError::Reset(String::from("Returned data too short")));
        }
        let mut middle = out.split_off(resp_templ.len());
        let resp = out;

        if resp != resp_templ {
            return Err(BusPirateError::Reset(format!("Found {:?} instead of {:?}", resp, resp_templ)));
        }

        let prompt = middle.split_off(middle.len() - prompt_templ.len());
        let version = middle;
        if prompt != prompt_templ {
            return Err(BusPirateError::Reset(format!("Found {:?} instead of {:?}", prompt, prompt_templ)));
        }

        Ok((version, BusPirate {
            port,
        }))
    }
    
    /// Try cooperatively
    fn port_into_bb(port: &mut serial::SystemPort) -> Result<(), BusPirateError> {
        BusPirate::port_command(port, b"\0", b"BBIO1")
    }

    fn port_command(
        port: &mut serial::SystemPort,
        cmd: &[u8],
        expect: &[u8],
    ) -> Result<(), BusPirateError> {
        port.write(cmd)?;
        port.flush()?;
        sleep(Duration::from_millis(10));
        let (out, result) = port.read_all();
        if let Err(e) = result {
            eprintln!("Warning: read problems when reconfiguring BP: {}", e);
        }
        if out.ends_with(expect) {
            Ok(())
        } else {
            Err(BusPirateError::Reconfigure(format!(
                "Found {:?} instead of {:?}", out, expect,
            )))
        }
    }

    fn into_bitbang(self) -> Result<BitBangPirate, (BusPirate, BusPirateError)> {
        BitBangPirate::new(self.port).map_err(|(port, e)| (
            BusPirate { port }, // Recreate the consumed self (how poetic)
            e,
        ))
    }
}

/// Arduino interface from progyon
trait Duino where Self: Sized {
    fn new(pirate: BusPirate) -> Result<Self, Error>;
    /// Feed bit stream
    fn feed(&mut self, stream: &[u8]) -> Result<Vec<bool>, Error> {
        let ret = Vec::new();
        for cmd in stream {
            match cmd {
                b'?' => println!("?"),
                b'5' => {
                    self.level_set(ICSPPins::Power)?;
                    self.input_set(ICSPPins::PGD)?;
                },
                b'.' => {},
                b'8' => sleep(Duration::from_millis(10)),
                b'3' => self.level_set(ICSPPins::MCLR)?,
                b'2' => self.level_clear(ICSPPins::MCLR)?,
                b'0' => {self.clock1()?;},
                other => panic!(format!("Unknown symbol: {:?}", other)),
            }
        }
        Ok(ret)
    }
    fn level_clear(&mut self, pins: ICSPPins) -> Result<(), Error>;
    fn level_set(&mut self, pins: ICSPPins) -> Result<(), Error>;
    fn input_set(&mut self, pins: ICSPPins) -> Result<(), Error>;

    fn clock1(&mut self) -> Result<bool, Error>;
}

/// Arduino interface from progyon, based on bitbanging.
struct BBDuino {
    pirate: BitBangPirate,
}

impl BBDuino {
    fn retry<F>(f: &mut F) -> Result<u8, Error>
        where F: FnMut() -> Result<u8, BusPirateError>
    {
        for _ in 0..5 {
            match f() {
                Err(BusPirateError::OutPinLevel { .. }) => {},
                other => return other.map_err(Error::BusPirate),
            }
        }
        Err(Error::BitBangRetry)
    }

    /// Not a clear idea why this is needed. Can't be bothered to analyze again.
    /// Probably something about not taking effect when done any other way.
    fn force_clear(&mut self, pin: u8) -> Result<u8, BusPirateError> {
        self.pirate.level_clear(pin)?;
        println!("fc");
        sleep(Duration::from_millis(100));
        self.pirate.input_clear(pin)
    }
}

impl Duino for BBDuino {
    fn new(pirate: BusPirate) -> Result<BBDuino, Error> {
        let mut pirate = pirate.into_bitbang()
            .map_err(|(_pirate, e)| {
                eprintln!("Golly, the pirate went overboard!");
                e
            })?;

        fn pb<E: Debug>(r: Result<u8, E>) -> Result<(), E> {
            match r {
                Ok(v) => Ok(println!("{:08b}", v)),
                Err(e) => {
                    println!("No value: {:?}", e);
                    Err(e)
                }
            }
        }

        pb(pirate.level_set(0))?;
        pb(pirate.level_set(BBPins::Power as u8))?;

        /// I have no idea what it does, but probably not pullup.
        fn pin_pullup(pirate: &mut BitBangPirate, pin: u8) -> Result<u8, Error> {
            pirate.level_clear(pin)?;
            println!("pull");
            sleep(Duration::from_millis(100));
            pirate.input_clear(pin)?;
            println!("pull");
            sleep(Duration::from_millis(100));
            pirate.level_set(pin)?;
            println!("pull");
            sleep(Duration::from_millis(100));
            let r = pirate.input_set(pin)?;
            Ok(r)
        }

        println!("D");
        sleep(Duration::from_millis(100));
        pb(pin_pullup(&mut pirate, ICSPPins::PGC as u8))?;
        println!("D");
        sleep(Duration::from_millis(100));
        pb(pin_pullup(&mut pirate, ICSPPins::PGD as u8))?;
        println!("D");
        let mut duino = BBDuino { pirate };
        sleep(Duration::from_millis(100));
        pb(duino.force_clear(ICSPPins::PGC as u8))?;
        sleep(Duration::from_millis(100));
        pb(duino.force_clear(ICSPPins::PGD as u8)); // this one fails a lot!
        pb(duino.force_clear(ICSPPins::PGD as u8))?;
        sleep(Duration::from_millis(100));
        println!("D");
        pb(duino.pirate.level_clear(ICSPPins::MCLR as u8))?;
        Ok(duino)
    }

    fn level_set(&mut self, pins: ICSPPins) -> Result<(), Error> {
        self.pirate.level_set(pins as u8)?;
        Ok(())
    }
    fn level_clear(&mut self, pins: ICSPPins) -> Result<(), Error> {
        self.pirate.level_clear(pins as u8)?;
        Ok(())
    }
    fn input_set(&mut self, pins: ICSPPins) -> Result<(), Error> {
        self.pirate.input_set(pins as u8)?;
        Ok(())
    }

    fn clock1(&mut self) -> Result<bool, Error> {
        panic!();
    }
}

enum ICSPPins {
    MCLR = 0b1,
    PGC = 0b100,
    PGD = 0b1000,
    Power = 0b1000000,
}

enum BBPins {
    Power = 0b1000000,
}

fn new_port(path: &OsString) -> Result<serial::SystemPort, serial::Error> {
    let mut port = serial::open(&path)?;
    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::BaudRate::Baud115200)?;
        //settings.set_char_size(serial::Bits8);
        //settings.set_parity(serial::ParityNone);
        //settings.set_stop_bits(serial::Stop1);
        //settings.set_flow_control(serial::FlowNone);
        Ok(())
    })?;
    Ok(port)
}

fn do_init(path: &OsString) -> Result<(), Error> {
    let mut port = serial::open(&path)?;
    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::BaudRate::Baud115200)?;
        //settings.set_char_size(serial::Bits8);
        //settings.set_parity(serial::ParityNone);
        //settings.set_stop_bits(serial::Stop1);
        //settings.set_flow_control(serial::FlowNone);
        Ok(())
    })?;
    let (version, pirate) = BusPirate::new(port)?;
    println!("Version: {}", String::from_utf8_lossy(&version));
    let mut duino = BBDuino::new(pirate)?;
    println!("Result: {:?}", duino.feed(b"?5.88888.32.8.0100.1101.0100.0011.0100.1000.0101.0000.8.3.8.eeeeedAIIIIIIIDDD"));
    Ok(())
}

#[derive(Debug)]
enum SerialFeedError {
    Serial(serial::Error),
    Execution(io::Error, usize),
}

fn feed_serial(path: OsString, data: &[seriali::Command])
    -> Result<Vec<seriali::Readout>, SerialFeedError>
{
    let mut port = new_port(&path).map_err(SerialFeedError::Serial)?;
    let (readouts, outcome) = seriali::execute(&mut port, data);
    outcome.map_err(|(e, line)| SerialFeedError::Execution(e, line))?;
    Ok(readouts)
}

fn init_bitbang(path: OsString) -> Result<buspirate::BitBang, Error> {
    let port = new_port(&path)?;
    let (version, pirate) = buspirate::Pirate::new(port)?;
    print_bytes(&version);
    let quirks = if common::is_subslice(&version, b"\r\nCommunity Firmware v7.1 ") {
        Some(buspirate::bitbang::Quirk::Fw71)
    } else {
        None
    };
    let bitbang = buspirate::BitBang::new(pirate, quirks)?;
    Ok(bitbang)
}

/// Watch enters a mode where every call is followed by a pause.
fn repl<C, I>(mut interpreter: I, data: &[C]) -> Result<(), Error>
    where C: DeserializeOwned + common::Command + Debug + Clone,
        I: Interpreter<C>,
{
    let stdin = io::stdin();
    let mut stdin = stdin.lock();
    for command in data {
        loop {
            let mut buf = String::new();
            print!("> {:?} ", command);
            std::io::stdout().flush().unwrap();
            stdin.read_line(&mut buf).unwrap();
            if buf.trim() == "" {
                break;
            }
            match common::streams::from_line(&buf) {
                Ok(c) => match interpreter.execute(&[c]) {
                    Ok(outcome) => print_stream_by_line(&outcome),
                    Err(e) => println!("Execution failure: {:?}", e),
                },
                Err(e) => println!("? {}", e),
            }
        }
        match interpreter.execute(&[command.clone()]) {
            Ok(outcome) => print_stream_by_line(&outcome),
            Err(e) => println!("Execution failure: {:?}", e),
        };
    }
    Ok(())
}

/// Watch enters a mode where every call is followed by a pause.
fn feed_bitbang(path: OsString, data: &[buspirate::bitbang::Command], watch: bool)
    -> Result<Vec<buspirate::Level>, Error>
{
    let mut bitbang = init_bitbang(path)?;
    Ok(if watch {
        let stdin = io::stdin();
        let mut stdin = stdin.lock();
        for command in data {
            loop {
                let mut buf = String::new();
                print!("> {:?} ", command);
                std::io::stdout().flush().unwrap();
                stdin.read_line(&mut buf).unwrap();
                if buf.trim() == "" {
                    break;
                }
                match buspirate::bitbang::streams::from_line(&buf) {
                    Ok(c) => {
                        let outcome = bitbang.execute(&[c.to_owned()])?;
                        print_levels(&outcome);
                    },
                    Err(e) => println!("? {}", e),
                }
            }
            let outcome = bitbang.execute(&[command.to_owned()])?;
            print_levels(&outcome);
        }
        Vec::new()
    } else {
        bitbang.execute(data)?
    })
}

fn transform_bitbang(data: &[buspirate::bitbang::Command])
    -> Result<Vec<seriali::Command>, ()>
{
    let (cmd, _pin) = buspirate::bitbang::transform_stream(
        data,
        buspirate::bitbang::PinConfigs { input_pins: 0x1f, high_pins: 0x60 }
    )?;
    Ok(cmd)
}

fn init_spi(path: OsString) -> Result<buspirate::spi::SPI, Error> {
    let port = new_port(&path)?;
    let (version, pirate) = buspirate::Pirate::new(port)?;
    print_bytes(&version);
    let spi = buspirate::spi::SPI::new(pirate)?;
    Ok(spi)
}

fn transform_spi(stream: &[buspirate::spi::Command], _config: &())
    -> Result<Vec<seriali::Command>, buspirate::spi::CompilationError>
{
    buspirate::spi::transform_stream(stream, ())
}        

fn transform_progduino_bitbang(
    data: &[progduino::Command],
    config: &progduino::Config,
) -> Result<Vec<buspirate::bitbang::Command>, progduino::CompilationError> {
    let cmds = progduino::transform_stream(data, config)?;
    Ok(cmds)
}

fn progduino_bitbang_default_pins() -> progduino::PinMapping {
    use progduino::Pin::*;
    use buspirate::bitbang::Pin as BPin;
    hashmap!(
        Power   => BPin::Power,
        PGD     => BPin::MOSI,
        PGC     => BPin::CLK,
        MCLR    => BPin::CS,
    )
}

fn transform_progduino_bitbang_pure(
    data: &[progduino::Command],
    _config: &(),
) -> Result<Vec<buspirate::bitbang::Command>, progduino::CompilationError> {
    transform_progduino_bitbang(
        data,
        &progduino::Config {
            pins: progduino_bitbang_default_pins(),
            quirks: None,
        }
    )
}

fn transform_progduino_bitbang_fw71(
    data: &[progduino::Command],
    _config: &(),
) -> Result<Vec<buspirate::bitbang::Command>, progduino::CompilationError> {
    use buspirate::bitbang::Pin as BPin;
    transform_progduino_bitbang(
        data,
        &progduino::Config {
            pins: progduino_bitbang_default_pins(),
            quirks: Some(progduino::Quirk::Fw71RaisePin(BPin::AUX)),
        }
    )
}

fn init_progduino_bitbang(
    path: OsString,
    quirks: Option<buspirate::bitbang::Quirks>,
) -> Result<progduino::BBPirate, Error> {
    let bitbang = init_bitbang(path)?;
    let r = progduino::BBPirate::new(
        bitbang,
        progduino_bitbang_default_pins(),
        quirks,
    )?;
    Ok(r)
}

fn init_progduino_bitbang_auto(path: OsString) -> Result<progduino::BBPirate, Error> {
    init_progduino_bitbang(path, None)
}

fn init_progduino_bitbang_none(path: OsString)
    -> Result<progduino::BBPirate, Error>
{
    init_progduino_bitbang(path, Some(None))
}

fn init_progduino_bitbang_fw71(path: OsString) -> Result<progduino::BBPirate, Error> {
    init_progduino_bitbang(path, Some(Some(buspirate::bitbang::Quirk::Fw71)))
}

fn feed_progduino_spi(path: OsString, data: &[progduino::Command], _watch: bool)
    -> Result<Vec<progduino::Bit>, Error>
{
    let port = new_port(&path)?;
    let (version, pirate) = buspirate::Pirate::new(port)?;
    print_bytes(&version);
    let spirate = buspirate::spi::SPI::new(pirate)?;
    let mut duino = progduino::SPIPirate::new(
        spirate,
    );
    let outcome = duino.execute(data).map_err(Error::ProgduinoSPI)?;
    Ok(outcome)
}


fn print_stream_by_line<T: Debug>(stream: &[T]) {
    for cmd in stream {
        println!("{:?}", cmd);
    }
}

fn print_bits(bits: &[progduino::Bit]) {
    for b in bits {
        println!(
            "{}",
            match b {
                progduino::Bit::Zero => "0",
                progduino::Bit::One => "1",
            },
        );
    }
}

/// TODO: actually correlate with pins
fn print_levels(levels: &[buspirate::Level]) {
    for level in levels {
        println!("{:?}", level);
    }
}

fn get_stream<C: DeserializeOwned, T>(
    name: &OsString,
    get_builtin: &dyn Fn(&str) -> Option<Vec<C>>,
    _: T,
) -> Option<Vec<C>> {
    let name = name.as_os_str().to_string_lossy().into_owned();
    match name.strip_prefix("builtin://") {
        Some(name) => get_builtin(name),
        None => File::open(name)
            .map(BufReader::new)
            .map_err(|e| eprintln!("File open error: {}", e))
            .and_then(|reader|
                common::streams::from_reader(reader)
                    .map_err(|e| eprintln!("Failed to load data: {}", e))
            )
            .ok()
    }
}

macro_rules! transform {
    (
        $args:expr,
        $stream_get:expr,
        $stream_read:expr,
        $compile:expr,
        $stream_write:expr,
    ) => {
        let stream = $args.next()
            .and_then(|name| get_stream(
                &name,
                &|name| $stream_get(name).map(Vec::from),
                &$stream_read,
            ));
        let serialize = $args.next() == Some(OsString::from("serialize"));
        match stream {
            Some(stream) => match $compile(&stream, &()) {
                Err(e) => eprintln!("Error: {:?}", e),
                Ok(outstream) => if serialize {
                    $stream_write(io::stdout(), &outstream).unwrap()
                } else {
                    print_stream_by_line(&outstream)
                },
            },
            _ => println!("Choose a valid stream name"),
        }
    }
}

macro_rules! interpret {
    ($args:expr, $stream_get:expr, $init:expr,) => {
        let stream = $args.next()
            .and_then(|name| get_stream(
                &name,
                &|name| $stream_get(name).map(Vec::from),
                (),
            ));

        let watch = $args.next() == Some(OsString::from("watch"));

        match stream {
            Some(stream) => match $args.next() {
                Some(path) => {
                    let mut interpreter = $init(path).unwrap_or_else(|e| {
                        eprintln!("Failed to init: {:?}", e);
                        panic!();
                    });
                    if watch {
                        repl(interpreter, &stream)
                            .unwrap_or_else(|e| eprintln!("Failed to exit: {:?}", e));
                    } else {
                        match interpreter.execute(&stream) {
                            Ok(o) => print_stream_by_line(&o),
                            Err(e) => eprintln!("Error: {:?}", e),
                        };
                    };
                },
                _ => println!("No/bad path given"),
            },
            _ => println!("Choose a valid stream name"),
        }
    }/*
    ($args:expr, $stream_get:expr, $feed:expr,) => {
        interpret!($args, $stream_get, $feed);
    };*/
}

fn main() {
    let mut args = env::args_os().skip(1);
    // First argument is a command, so lossy conversion just to match on it
    // is ok.
    let cmd = args.next().map(|s| s.as_os_str().to_string_lossy().into_owned());
    match (&cmd).as_ref().map(String::as_str) {
        Some("pirate") => {
            for arg in env::args_os().skip(2) {
                if let Err(e) = do_init(&arg) {
                    eprintln!("Failed due to {:?}", e);
                }
            }
        },
        Some("serial:interpret") => {
            // All streams to choose from currently relevant to BusPirate bitbang mode.
            // TODO: when that changes, maybe namespace them.
            let stream = args.next()
                .and_then(|name| get_stream(
                    &name,
                    &|name| buspirate::serialstreams::get(name).map(Vec::from),
                    (),
                ));

            match stream {
                Some(stream) => match args.next() {
                    Some(path) => match feed_serial(path, &stream) {
                        Err(e) => eprintln!("Error: {:?}", e),
                        Ok(o) => seriali::print_readouts(&o),
                    },
                    _ => println!("No/bad path given"),
                },
                _ => println!("Choose a valid stream name"),
            }
        },
        Some("bitbang:init") => {
            match args.next() {
                Some(path) => {
                    match init_bitbang(path) {
                        Err(e) => eprintln!("Error: {:?}", e),
                        Ok(_) => {},
                    }
                },
                _ => println!("No path given"),
            }
        },
        Some("bitbang:interpret") => {
            let stream = args.next()
                .and_then(|name| get_stream(
                    &name,
                    &|name| buspirate::bitbang::streams::get(name).map(Vec::from),
                    (),
                ));

            let watch = args.next() == Some(OsString::from("watch"));

            match stream {
                Some(stream) => match args.next() {
                    Some(path) => match feed_bitbang(path, &stream, watch) {
                        Err(e) => eprintln!("Error: {:?}", e),
                        Ok(o) => print_levels(&o),
                    },
                    _ => println!("No/bad path given"),
                },
                _ => println!("Choose a valid stream name"),
            };
        },
        Some("bitbang:transform") => {
            let name = args.next().map(|s| s.as_os_str().to_string_lossy().into_owned());
            let stream = (&name).as_ref().map(String::as_str)
                .and_then(buspirate::bitbang::streams::get);
            match stream {
                Some(stream) => match transform_bitbang(stream) {
                    Err(e) => eprintln!("Error: {:?}", e),
                    Ok(o) => print_stream_by_line(&o),
                },
                _ => println!("Choose a valid stream name"),
            };
        },
        Some("spi:interpret") => {
            interpret!(
                args,
                buspirate::spi::streams::get,
                init_spi,
            );
        },
        Some("spi:transform") => {
            transform!(
                args,
                buspirate::spi::streams::get,
                (),
                transform_spi,
                common::streams::to_writer,
            );
        },
        Some("progduino:bitbang:transform") => {
            transform!(
                args,
                progduino::streams::get,
                (),
                transform_progduino_bitbang_pure,
                common::streams::to_writer,
            );
        },
        Some("progduino:bitbang71:transform") => {
            transform!(
                args,
                progduino::streams::get,
                (),
                transform_progduino_bitbang_fw71,
                common::streams::to_writer,
            );
        },
        Some("progduino:spi:transform") => {
            use progduino::Compiler;
            let stream = args.next()
                .and_then(|name| get_stream(
                    &name,
                    &|name| progduino::streams::get(name).map(Vec::from),
                    (),
                ));
            let serialize = args.next() == Some(OsString::from("serialize"));
            match stream {
                Some(stream) => match progduino::SPIBackend::transform_stream(&stream, &()) {
                    Err(e) => eprintln!("Error: {:?}", e),
                    Ok(outstream) => if serialize {
                        common::streams::to_writer(io::stdout(), &outstream).unwrap()
                    } else {
                        print_stream_by_line(&outstream)
                    },
                },
                _ => println!("Choose a valid stream name"),
            };
        },
        Some("progduino:bitbang:interpret") => {
            interpret!(
                args,
                progduino::streams::get,
                init_progduino_bitbang_auto,
            );
        },
        Some("progduino:bitbang:interpret?") => {
            interpret!(
                args,
                progduino::streams::get,
                init_progduino_bitbang_none,
            );
        },
        Some("progduino:bitbang:interpret?fw71") => {
            interpret!(
                args,
                progduino::streams::get,
                init_progduino_bitbang_fw71,
            );
        },
        Some("progduino:spi:interpret") => {
            let stream = args.next()
                .and_then(|name| get_stream(
                    &name,
                    &|name| progduino::streams::get(name).map(Vec::from),
                    (),
                ));

            let watch = args.next() == Some(OsString::from("watch"));

            match stream {
                Some(stream) => match args.next() {
                    Some(path) => match feed_progduino_spi(path, &stream, watch) {
                        Err(e) => eprintln!("Error: {:?}", e),
                        Ok(o) => print_bits(&o),
                    },
                    _ => println!("No/bad path given"),
                },
                _ => println!("Choose a valid stream name"),
            };
        },
        _ => println!("Choose a valid command"),
    }
}
