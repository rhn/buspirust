use serial;
use std::io;
use std::thread::sleep;
use std::time::Duration;
use super::seriali;


use serial::SerialPort;
use std::io::Write;
use super::ReadAll;


pub mod spi;
pub mod bitbang;

pub use bitbang::BitBang;


#[derive(Debug)]
pub enum DeviceError {
    BBInit(&'static str),
    Start(String),
    Command(String),
    IO(io::Error),
}

impl From<io::Error> for DeviceError {
    fn from(e: io::Error) -> DeviceError {
        DeviceError::IO(e)
    }
}

/// The general BP interface.
pub struct Pirate {
    port: serial::SystemPort,
}

impl Pirate {
    pub fn new(mut port: serial::SystemPort)
        -> Result<(Vec<u8>, Pirate), DeviceError> // return version buffer alongside
    {
        // Timeout of 10ms led to lost writes (?).
        if let Err(e) = port.set_timeout(Duration::from_millis(20)) {
            eprintln!("Warning: timeout not set. May be slow or unstable. {}", e);
        }
        /* TODO: check pid/vid from
         * /sys/class/tty/ttyUSB0/device/subsystem -> ../../../../../../../bus/usb-serial
         * then realpath it to get down to /sys/devices/pci0000:00/0000:00:14.0/usb3/3-1/
         * then read idProduct and IdVendor.
         * 
         * Then read /usr/lib/python3.7/site-packages/serial/tools/list_ports*
         */
        let mut reset = || {
            for _i in 0..20 {
                if let Ok(()) = port::command_quick(&mut port, b"\x00", b"BBIO1") {
                    return Some(());
                }
            }
            None
        };
        reset().ok_or(DeviceError::BBInit("Reset sequence failed to give bitbang mode"))?;

        port::command(&mut port, b"\x0f", b"\x01", Duration::from_millis(100))?;

        let (mut out, result) = port.read_all();
        if let Err(e) = result {
            eprintln!("Warning: read problems when resetting BP: {}", e);
        }

        let prompt_templ = b"HiZ>";
        let prompt = out.split_off(out.len() - prompt_templ.len());
        let version = out;
        if prompt != prompt_templ {
            return Err(DeviceError::Start(format!("Found {:?} instead of {:?}", prompt, prompt_templ)));
        }

        Ok((version, Pirate {
            port,
        }))
    }
}

#[derive(Debug)]
pub enum Level {
    High,
    Low,
}

mod port {
    // TODO: this should be replaced by seriali
    use super::*;
    // TODO: Pile them up and throw into init() or sth
    pub fn input_put(p: &mut serial::SystemPort, pins: u8) -> io::Result<u8> {
        p.write(&[0x40 | (pins & 0x1f)])?;
        p.flush()?; // this is kinda silly. Should be turned async (command stream?)
        sleep(Duration::from_millis(10));
        consume_pins_byte(p)
    }

    pub fn level_put(p: &mut serial::SystemPort, pins: u8) -> io::Result<u8> {
        p.write(&[0x80 | (pins & 0x7f)])?;
        p.flush()?;
        sleep(Duration::from_millis(10));
        consume_pins_byte(p)
    }

    /// Interprets the single byte with pin info that was left
    /// just after a call to change pin state.
    fn consume_pins_byte(p: &mut serial::SystemPort) -> io::Result<u8> {
        let bytes = p.read_exact_vec(1)?;
        Ok(bytes[0] & 0x7f)
    }
    // END TODO

    pub fn command_quick(
        p: &mut serial::SystemPort,
        cmd: &[u8],
        expect: &[u8],
    ) -> Result<(), DeviceError> {
        port::command(p, cmd, expect, Duration::from_millis(10))
    }

    /// Executes command, waits a moment, expects a particular output.
    pub fn command(
        p: &mut serial::SystemPort,
        cmd: &[u8],
        expect: &[u8],
        delay: Duration,
    ) -> Result<(), DeviceError> {
        p.write(cmd)?;
        p.flush()?;
        sleep(delay);
        let out = p.read_exact_vec(expect.len())?;
        if out.ends_with(expect) {
            Ok(())
        } else {
            Err(DeviceError::Command(format!(
                "Found {:?} instead of {:?}", out, expect,
            )))
        }
    }
}


pub mod serialstreams {
    use std::time::Duration;
    use super::seriali;
    use super::seriali::Command::*;

    pub fn get(name: &str) -> Option<Vec<seriali::Command>> {
        match name {
            "info" => Some(vec!(
                Send(b'i'),
                Send(b'\n'),
                Flush,
                Read(1),
                ReadUntil(b">".iter().map(|x| *x).collect()),
            )),
            "into_bbio" => Some(vec!(
                // Bring to a state where a known number of \0s works.
                Send(b'i'),
                Send(b'\n'),
                Flush,
                Read(1),
                ReadUntil(b">".iter().map(|x| *x).collect()),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Flush,
                Read(5),
            )),
            // Not more than 20 and the BP should be back in BBIO mode.
            // First success counts.
            "try_reset" => Some(vec!(
                Send(b'\x00'),
                Flush,
                Read(5),
            )),
            // If you get 0x1, you win.
            "stop_bbio_0x1" => Some(vec!(
                Send(b'\x0f'),
                Flush,
                Wait(Duration::from_millis(200)),
                Read(1),
            )),
            // Changes into BBIO mode regardless if in BBIO or HiZ mode
            "start_bbio_safe" => Some(vec!(
                Send(b'\x0f'), // HiZ prints \x7, BBIO resets to HiZ and prints info
                Flush,
                Wait(Duration::from_millis(200)),
                Read(1),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Send(b'\x00'),
                Flush,
                // skips info if printed
                ReadUntil(b"BBIO1".iter().map(|x| *x).collect()),
            )),
            "spi_echo" => Some(vec!(
                Send(0x1),
                Read(4), // SPI1
                Send(b'\x11'), // Xfer 2B
                Send(5),
                Send(4),
                Flush,
                Read(3), // 0x01 5 4
            )),
            _ => None,
        }
    }
}
